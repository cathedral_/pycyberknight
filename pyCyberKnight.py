##################
# Import modules #
##################
from Queue import Queue  # Modules for Queuing threaded processes
from Queue import Empty
from threading import Thread  # Module for threading processes
import time  # Needed to syncronize process communications
import subprocess, \
    os  # subprocess module for launching processes, probably don't need os anymore due to revision in shell_source()
from tqdm import tqdm
import commands

################################
# Set up some global variables #
################################
# num_fetch_threads = 6  # Defines how many threads we want to launch.
q = Queue()  # Needed to setup the que
vuln_ids = []  # Our placeholder array - read checklist XML and place into this array (more correctly a python list)
extension_add = '.sh'  # An extension we add to the vuln_id's placed in the above list.
script = ''  # init our finished path name (which would be script = pathname + vulnid + .sh


##########################################################################################
# Worker function.                                                                       #
# It takes three variables, thread count, the que count, and the complete script name,   # 
# and then returns two (finding_details & finding_status)                                #
##########################################################################################
def shell_source(q):
    for job in tqdm(range(q.qsize())):  # This is to add a progress bar. For such a fluff item, it's a PITA.
        working_vuln = q.get()
        check_vuln(working_vuln)


def check_vuln(working_vuln):
    try:
        # I use two variables, x & y to navigate the tree. The DISA checklists XML requires special navigating due to
        # it's non XML standard compliance.
        y = 0
        prepend = "V-"
        DEVNULL = open(os.devnull, 'wb')
        pipe = subprocess.Popen(". %s >> /dev/null; echo \"$STATUS\";echo \"$FINDING_DETAILS\"" % working_vuln,
                                bufsize=-1, stdout=subprocess.PIPE, stderr=DEVNULL, universal_newlines=False,
                                shell=True)
        output = pipe.communicate()[0]  # Get the output of the command above
        finding_status = output.splitlines()[0]
        # Take the first line of the globbed output which is $STATUS and format it as a string.
        finding_details = output.splitlines()[1:]
        # Take the rest of the globbed output which is $FINDING_DETAILS and store it as a list.
        # Because DISA sucks I have to iterate through the <VULN> tag to actually find the
        # correct one to insert $STATUS and $FINDING_DETAILS into.
        # Seems they can't keep to a standard XML structure.
        while True:  # Loop through the XML code and pull vulnerability ID's
            working_temp, vulnid = working_vuln.split('-')
            # We put all the variables together into one string for the que, so now we have to strip it again.
            new_vuln = prepend + vulnid[:-3]
            if new_vuln == root[1][0][y][0][1].text:
                # Find the child with the vulnerability ID that matches (i.e., V-1025)
                for item in range(30):
                    # I had to do this because the structure of the children in the XML checklist aren't always the same
                    if root[1][0][y][item].tag == 'STATUS':
                        # print "Found STATUS, updating..."
                        root[1][0][y][item].text = finding_status
                        # update the correct item. I wish there was a better way to do this.
                    if root[1][0][y][item].tag == 'FINDING_DETAILS':
                        # print "Found FINDING_DETAILS, updating ...."
                        root[1][0][y][item].text = '\n'.join(finding_details)
                        # because this variable is a list, we had to convert it a block of strings.
                break
            else:
                y += 1  # increase the count.

    except:
        # Going to have to comment this out, as we are iterating through the tree, lots of index errors are raised.
        # I wish I knew of a better way to find XML nodes, and update them, but this is what I have for now.
        # It raises lots of errors that we just ignore.
        # print ("Caught an index error, something is wrong....")
        pass
    q.task_done()


###################################################################
# This function calls the checklist, reads the entire thing into  #
# memory, and then loads our vulnerability ids.                   #
# I believe Mike would like a menu here, as to select the proper  #
# checklist rather than having the user input it. Probably a      #
# good idea.                                                      #
###################################################################
def get_vuln_ids():
    import xml.etree.ElementTree as ET  # Import XML ElementTree module
    global checklist
    checklist = raw_input("Enter the name of the checklist to process (must be in same directory as CyberKnight) --> ")
    global tree
    global root
    tree = ET.parse(checklist)
    root = tree.getroot()  # initialize the tree.
    x = 1
    while True:  # Loop through the XML code and pull vulnerability ID's
        try:
            result = root[1][0][x][0][1].text
            # ElementTree indexes elements via this tag. Notice the 'x' variable, that it how we are iterating through.
            vuln_ids.append(result.lower())  # change all V-ID to lowercase v-id to match shell scripts.
            x += 1  # increase the count.
        except IndexError:
            # for some reason after the loop is finished, it throws an exception. This is how we handle it.
            print("Array Loaded.")  # Notify the user the array has been loaded.
            break  # break the function (due to an exception being raised and continue with execution).


###############################################################
# Once we've got all of our data V-ID's loaded into a list    #
# we can send them to the que for processing.                 #
###############################################################
def send_to_que():  # Get Vulnerability ID's, append .sh and send to que for processing
    for script in iter(vuln_ids):
        script = Pathname + '/scripts/' + script + extension_add
        q.put(script)
        # This deserves some discussion:
        # Right now I'm kind of cheating and using the que for two things instead of just one:
        # 1.) To fill up the que
        # 2.) As a list to absolute paths to scripts.
        # It's #2 I have a problem with. It causes all sorts of problems when using a progress bar.
        # Most progress bars that can utilize a que in a multi-threaded program rely on integers
        # in the que to mark progress and they flip out when the item in the que is a string. I'd like to change the
        # code so we are doing it properly, but it will take some work.
        # Logic like the following might work -
        # for items in range(vuln_ids):
        # 	q.put(items)
        # This is probably the correct way to do it, but now I have to match que items to actual vulnerability ids in
        # another function.
        # I could probably do this in shell_source() as something like
        # id = q.get() which would give me the que integer
        # then enumerate(vuln_ids) and somehow compare id and match with the enumerated list,
        # but as for now I'm kinda stuck.


#############################
# Start the worker threads  #
#############################
def worker_threads():  # Set up some threads to run our scripts
    num_fetch_threads = int(raw_input("How many threads to launch? (Max 10) --> "))
    for i in range(num_fetch_threads):
        worker = Thread(target=shell_source, args=(q,))
        # Call to our shell_source function, which launches the shell scripts.
        worker.setDaemon(True)
        worker.start()  # Start our workers.


########################################
# Write our finished checklist to disk #
########################################
def write_checklist():
    print('\n' * 15)
    finished_checklist = checklist[:-4] + "_finished.ckl"
    print "*** Saving updated Checklist as %s" % finished_checklist
    tree.write(finished_checklist)


#######################################
# Mike's nifty CyberKnight logo       #
#######################################

def print_logo():
    print("""\
	
	               ,   A           {}
	              / \, | ,        .--.
	             |    =|= >      /.--.\\
	              \ /` | `       |====|
	               `   |         |`::`|
	                   |     .-;`\..../`;_.-^-._
	                  /\\/  /  |...::..|`   :   `|
	                  |: \ |   /...::..|   .:.   |
	                   \ /\;-,/\   ::  |..:::::..|
	                   |\ <` >  >._::_.| .:::::. |
	                   | `..`  /   ^^  |   .:.   |
	                   |       |       \    :    /
	                   |       |        \   :   /
	                   |       |___/\___|`-.:.-`
	                   |        \_ || _/    `
	                   |        <_ >< _>
	                   |        |  ||  |
	                   |        |  ||  |
	                   |       _\.:||:./_
	                   |      /____/\____\\
	
	                           CyberKnight
	
	""")


def get_asset():
    my_host = os.uname()[1]
    print "CyberKnight detects the current hostname as * %s *" % my_host
    question1 = raw_input("Is this correct? (Y/N) --> ")
    if question1 in ['n', 'N', 'no', 'No', 'NO']:
        my_host = raw_input("Then what is the hostname of the system? --> ")
    ips = commands.getoutput(
        "/sbin/ifconfig | grep -i \"inet\" | grep -iv \"inet6\" | " + "awk {'print $2'} | sed -ne 's/addr\:/ /p'")
    print ("CyberKnight detects the following IP Addresses on the system: \n")
    print ips
    print
    my_ip = raw_input("Input the correct IP Address: --> ")
    # from uuid import getnode as get_mac #### this snippet of code is a work in progress
    # mac = get_mac() ### Attempting to get multiple macs and figure out which one is our target interface
    # ':'.join(("%012X" % mac)[i:i+2] for i in range(0, 12, 2))
    print ("*** Notice - You will have to put in the MAC Address manually. ")
    root[0][1].text = my_host
    root[0][2].text = my_ip


if __name__ == '__main__':
    #############################
    # Do some sanity checking   #
    #############################
    # 9/2/2016 - Need to add a clean up function to the code. We need to move our stuff to the correct directories etc.

    Pathname = os.getcwd()  # Get current path
    temp_dir = Pathname + '/temp'  # Setup our temp directory for our scripts.
    scripts_dir = Pathname + '/scripts'  # Check to make sure our scripts are present.
    if not os.path.exists(temp_dir):  # Ensure directory exists, if not create it.
        os.makedirs(temp_dir)
    if not os.path.exists(scripts_dir):
        print ""
        print "*** No scripts found! Cannot continue without scripts."
        print "*** Please put CyberKnight bash scripts into the same directory as CyberKnight is located."
        print "*** Now Exiting..."
        print ""
        # import os
        os._exit(1)

    ###################################
    # Begin Main Execution            #
    ###################################

    print_logo()
    get_vuln_ids()
    get_asset()
    send_to_que()
    worker_threads()
    # for i in shell_source(q):
    # pass
    q.join()
    write_checklist()
    print '*** Done!'
