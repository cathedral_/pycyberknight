

	               ,   A           {}
	              / \, | ,        .--.
	             |    =|= >      /.--.\
	              \ /` | `       |====|
	               `   |         |`::`|
	                   |     .-;`\..../`;_.-^-._
	                  /\/  /  |...::..|`   :   `|
	                  |: \ |   /...::..|   .:.   |
	                   \ /\;-,/\   ::  |..:::::..|
	                   |\ <` >  >._::_.| .:::::. |
	                   | `..`  /   ^^  |   .:.   |
	                   |       |       \    :    /
	                   |       |        \   :   /
	                   |       |___/\___|`-.:.-`
	                   |        \_ || _/    `
	                   |        <_ >< _>
	                   |        |  ||  |
	                   |        |  ||  |
	                   |       _\.:||:./_
	                   |      /____/\____\
	
	                           pyCyberKnight

# Quick summary #

* pyCyberKnight is a python based, multi-threaded, native XML parsing, optimized cyber security tool for scanning and auditing target systems. 
* It uses a python based core engine combined with custom bash scripts to perform DISA STIG Checklist auditing. 
* pyCyberKnight is currently supported for Red Hat Enterprise Linux versions 5, 6, 7 32/64 bit, Sun Solaris 10 x86 (SPARC coming soon!), and CentOS 5,6,7 32/64. 

# Version #

* pyCyberKnight is currently version 0.5 (beta). The code works on the target platform(s) but requires custom bash scripts to be written. 


### How do I get set up? ###

* pyCyberKnight requires minimum python 2.7.3. It also requires the actual shell scripts to perform the target work. pyCyberKnight is only the multi-threaded engine that enables the scripts to run in a multi-threaded environment. It also parses DISA checklists to read in, and write out audit information. 
* The best configuration of pyCyberKnight for auditing a target system is using the cxfreeze python module. Ideally, you would want to mimick the target environment, install a python 2.7 virtual environment, then cxfreeze the script and install the standalone version on the TOE. 
* Dependencies - python 2.7 and the following python modules: Queue, tqdm, subprocess, thread
* How to run tests - to be written later. 
* Deployment instructions - to be written later. 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact